**This documentation is automatically generated.**

**Output schemas only represent `data` and not the full output; see output examples and the JSend specification.**

# /api/animal/all/?

    Content-Type: application/json

## GET


**Input Schema**
```json
null
```



**Output Schema**
```json
{
    "properties": {
        "animals": {
            "items": {
                "properties": {
                    "age": {
                        "type": "number"
                    },
                    "id": {
                        "type": "number"
                    },
                    "likes": {
                        "type": "number"
                    },
                    "name": {
                        "type": "string"
                    },
                    "race": {
                        "type": "string"
                    }
                },
                "type": "object"
            },
            "minItems": 0,
            "type": "array"
        }
    },
    "type": "object"
}
```


**Output Example**
```json
{
    "animals": [
        {
            "age": 1,
            "id": 1,
            "likes": 2,
            "name": "Stefan",
            "race": "Zebra"
        },
        {
            "age": 1,
            "id": 2,
            "likes": 2,
            "name": "Zbigniew",
            "race": "Lew"
        }
    ]
}
```


**Notes**

GET all Animals data



<br>
<br>

# /api/animal/animal/\(?P\<pk\>\[a\-zA\-Z0\-9\_\\\-\]\+\)/?$

    Content-Type: application/json

## GET


**Input Schema**
```json
null
```



**Output Schema**
```json
{
    "properties": {
        "age": {
            "type": "number"
        },
        "id": {
            "type": "number"
        },
        "likes": {
            "type": "number"
        },
        "name": {
            "type": "string"
        },
        "race": {
            "type": "string"
        }
    },
    "type": "object"
}
```


**Output Example**
```json
{
    "age": 1,
    "id": 1,
    "likes": 2,
    "name": "Stefan",
    "race": "Zebra"
}
```


**Notes**

GET Animal data
* `pk`: ID of the animal



<br>
<br>

# /api/animal/animal/\(?P\<pk\>\[a\-zA\-Z0\-9\_\\\-\]\+\)/?$

    Content-Type: application/json

## POST


**Input Schema**
```json
{
    "properties": {
        "age": {
            "type": "number"
        },
        "likes": {
            "type": "number"
        },
        "name": {
            "type": "string"
        },
        "race": {
            "type": "string"
        }
    },
    "type": "object"
}
```


**Input Example**
```json
{
    "age": 1,
    "likes": 2,
    "name": "Stefan",
    "race": "Zebra"
}
```


**Output Schema**
```json
{
    "properties": {
        "message": {
            "type": "string"
        }
    },
    "type": "object"
}
```


**Output Example**
```json
{
    "message": "Animal<1: Stefan> created"
}
```


**Notes**

POST the required parameters to create a new Animal
* `name`: Name of the animal
* `race`: Race of the animal
* `age`: Age of the animal
* `likes`: Likes of the animal



## PUT


**Input Schema**
```json
{
    "properties": {
        "age": {
            "type": "number"
        },
        "likes": {
            "type": "number"
        },
        "name": {
            "type": "string"
        },
        "race": {
            "type": "string"
        }
    },
    "type": "object"
}
```


**Input Example**
```json
{
    "age": 1,
    "likes": 2,
    "name": "Edward",
    "race": "Zebra"
}
```


**Output Schema**
```json
{
    "properties": {
        "message": {
            "type": "string"
        }
    },
    "type": "object"
}
```


**Output Example**
```json
{
    "message": "Animal<1: Edward> updated"
}
```


**Notes**

Put the required parameters to update Animal
:param pk to identify animal to update
* `name`: Name of the animal
* `race`: Race of the animal
* `age`: Age of the animal
* `likes`: Likes of the animal



## DELETE


**Input Schema**
```json
null
```



**Output Schema**
```json
{
    "properties": {
        "message": {
            "type": "string"
        }
    },
    "type": "object"
}
```


**Output Example**
```json
{
    "message": "Animal<1: Edward> removed"
}
```


**Notes**

DELETE method to remove animal
:param pk: identify animal to remove



<br>
<br>

# /api/animal/animal/?

    Content-Type: application/json

## POST


**Input Schema**
```json
{
    "properties": {
        "age": {
            "type": "number"
        },
        "likes": {
            "type": "number"
        },
        "name": {
            "type": "string"
        },
        "race": {
            "type": "string"
        }
    },
    "type": "object"
}
```


**Input Example**
```json
{
    "age": 1,
    "likes": 2,
    "name": "Stefan",
    "race": "Zebra"
}
```


**Output Schema**
```json
{
    "properties": {
        "message": {
            "type": "string"
        }
    },
    "type": "object"
}
```


**Output Example**
```json
{
    "message": "Animal<1: Stefan> created"
}
```


**Notes**

POST the required parameters to create a new Animal
* `name`: Name of the animal
* `race`: Race of the animal
* `age`: Age of the animal
* `likes`: Likes of the animal



## PUT


**Input Schema**
```json
{
    "properties": {
        "age": {
            "type": "number"
        },
        "likes": {
            "type": "number"
        },
        "name": {
            "type": "string"
        },
        "race": {
            "type": "string"
        }
    },
    "type": "object"
}
```


**Input Example**
```json
{
    "age": 1,
    "likes": 2,
    "name": "Edward",
    "race": "Zebra"
}
```


**Output Schema**
```json
{
    "properties": {
        "message": {
            "type": "string"
        }
    },
    "type": "object"
}
```


**Output Example**
```json
{
    "message": "Animal<1: Edward> updated"
}
```


**Notes**

Put the required parameters to update Animal
:param pk to identify animal to update
* `name`: Name of the animal
* `race`: Race of the animal
* `age`: Age of the animal
* `likes`: Likes of the animal



## DELETE


**Input Schema**
```json
null
```



**Output Schema**
```json
{
    "properties": {
        "message": {
            "type": "string"
        }
    },
    "type": "object"
}
```


**Output Example**
```json
{
    "message": "Animal<1: Edward> removed"
}
```


**Notes**

DELETE method to remove animal
:param pk: identify animal to remove



<br>
<br>

# /api/animal/animals/?

    Content-Type: application/json

## POST


**Input Schema**
```json
{
    "properties": {
        "new_animals": {
            "items": {
                "properties": {
                    "age": {
                        "type": "number"
                    },
                    "likes": {
                        "type": "number"
                    },
                    "name": {
                        "type": "string"
                    },
                    "race": {
                        "type": "string"
                    }
                },
                "type": "object"
            },
            "minItems": 1,
            "type": "array"
        }
    },
    "type": "object"
}
```


**Input Example**
```json
{
    "new_animals": [
        {
            "age": 1,
            "likes": 2,
            "name": "Stefan",
            "race": "Zebra"
        }
    ]
}
```


**Output Schema**
```json
{
    "properties": {
        "message": {
            "type": "string"
        }
    },
    "type": "object"
}
```


**Output Example**
```json
{
    "message": "All Animals created"
}
```


**Notes**

POST the required parameters to create few new Animals instance.
Each instance need this parameters
* `name`: Name of the animal
* `race`: Race of the animal
* `age`: Age of the animal
* `likes`: Likes of the animal


