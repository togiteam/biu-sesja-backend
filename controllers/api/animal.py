from sqlalchemy import update
from sqlalchemy.orm import sessionmaker
from tornado_json import schema
from tornado_json.requesthandlers import APIHandler
from tornado.web import HTTPError

from models.animal import Animal
from settings.settings import engine

SESSION = sessionmaker(bind=engine)


class AnimalApi(APIHandler):
    __url_names__ = ["animal"]


class AnimalsApi(APIHandler):
    __url_names__ = ["animals"]


class GetAnimal(AnimalApi):
    @schema.validate(
        output_schema={
            "type": "object",
            "properties": {
                "id": {
                    "type": "number"
                },
                "name": {
                    "type": "string"
                },
                "race": {
                    "type": "string"
                },
                "age": {
                    "type": "number"
                },
                "likes": {
                    "type": "number"
                }
            }
        },
        output_example={
            "id": 1,
            "name": "Stefan",
            "race": "Zebra",
            "age": 1,
            "likes": 2
        }
    )
    def get(self, pk):
        """
        GET Animal data
        * `pk`: ID of the animal
        """
        session = SESSION()
        animal = session.query(Animal).filter_by(id=pk).first()
        session.commit()
        return {
            "id": animal.id,
            "name": animal.name,
            "race": animal.race,
            "age": animal.age,
            "likes": animal.likes
        }


class CreateAnimal(AnimalApi):
    @schema.validate(
        input_schema={
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "race": {
                    "type": "string"
                },
                "age": {
                    "type": "number"
                },
                "likes": {
                    "type": "number"
                }
            }
        },
        input_example={
            "name": "Stefan",
            "race": "Zebra",
            "age": 1,
            "likes": 2
        },
        output_schema={
            "type": "object",
            "properties": {
                "message": {
                    "type": "string"
                }
            }
        },
        output_example={
            "message": "Animal<1: Stefan> created"
        }
    )
    def post(self):
        """
        POST the required parameters to create a new Animal
        * `name`: Name of the animal
        * `race`: Race of the animal
        * `age`: Age of the animal
        * `likes`: Likes of the animal
        """
        # `schema.validate` will JSON-decode `self.request.body` for us
        #   and set self.body as the result, so we can use that here
        new_animal = Animal(
            name=self.body.get('name'),
            race=self.body.get('race'),
            age=self.body.get('age') if self.body.get('age') else 0,
            likes=self.body.get('likes') if self.body.get('likes') else 0
        )
        session = SESSION()
        session.add(new_animal)
        session.commit()
        return {
            "message": "Animal<{}: {}> created".format(new_animal.id, new_animal.name)
        }

    @schema.validate(
        input_schema={
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "race": {
                    "type": "string"
                },
                "age": {
                    "type": "number"
                },
                "likes": {
                    "type": "number"
                }
            }
        },
        input_example={
            "name": "Edward",
            "race": "Zebra",
            "age": 1,
            "likes": 2
        },
        output_schema={
            "type": "object",
            "properties": {
                "message": {
                    "type": "string"
                }
            }
        },
        output_example={
            "message": "Animal<1: Edward> updated"
        }
    )
    def put(self, pk=None):
        """
        Put the required parameters to update Animal
        :param pk to identify animal to update
        * `name`: Name of the animal
        * `race`: Race of the animal
        * `age`: Age of the animal
        * `likes`: Likes of the animal
        """
        session = SESSION()
        animal = session.query(Animal).filter(Animal.id == pk).first()
        animal.name = self.body.get('name')
        animal.race = self.body.get('race')
        animal.age = self.body.get('age')
        animal.likes = self.body.get('likes')
        session.commit()
        return {
            "message": "Animal<{}: {}> updated".format(animal.id, animal.name)
        }

    @schema.validate(
        output_schema={
            "type": "object",
            "properties": {
                "message": {
                    "type": "string"
                }
            }
        },
        output_example={
            "message": "Animal<1: Edward> removed"
        }
    )
    def delete(self, pk=None):
        """
        DELETE method to remove animal
        :param pk: identify animal to remove
        """
        session = SESSION()
        animal_to_remove = session.query(Animal).filter(Animal.id == pk).first()
        name = animal_to_remove.name
        session.delete(animal_to_remove)
        session.commit()
        return {
            "message": "Animal<{}: {}> removed".format(pk, name)
        }


class GetAnimals(AnimalsApi):
    __url_names__ = ["all"]

    @schema.validate(
        output_schema={
            "type": "object",
            "properties": {
                "animals": {
                    "type": "array",
                    "minItems": 0,
                    "items": {
                        "type": "object",
                        "properties": {
                            "id": {
                                "type": "number"
                            },
                            "name": {
                                "type": "string"
                            },
                            "race": {
                                "type": "string"
                            },
                            "age": {
                                "type": "number"
                            },
                            "likes": {
                                "type": "number"
                            }
                        }
                    }
                }
            }
        },
        output_example={
            "animals": [
                {
                    "id": 1,
                    "name": "Stefan",
                    "race": "Zebra",
                    "age": 1,
                    "likes": 2
                },
                {
                    "id": 2,
                    "name": "Zbigniew",
                    "race": "Lew",
                    "age": 1,
                    "likes": 2
                }
            ]
        }
    )
    def get(self):
        """
        GET all Animals data
        """
        session = SESSION()
        animals = session.query(Animal).all()
        session.commit()
        result = []

        for animal in animals:
            result.append({
                "id": animal.id,
                "name": animal.name,
                "race": animal.race,
                "age": animal.age,
                "likes": animal.likes
            })

        return {"animals": result}


class CreateAnimals(AnimalsApi):
    @schema.validate(
        input_schema={
            "type": "object",
            "properties": {
                "new_animals": {
                    "type": "array",
                    "minItems": 1,
                    "items": {
                        "type": "object",
                        "properties": {
                            "name": {
                                "type": "string"
                            },
                            "race": {
                                "type": "string"
                            },
                            "age": {
                                "type": "number"
                            },
                            "likes": {
                                "type": "number"
                            }
                        }
                    }
                }
            }
        },
        input_example={
            "new_animals": [
                {
                    "name": "Stefan",
                    "race": "Zebra",
                    "age": 1,
                    "likes": 2
                }
            ]
        },
        output_schema={
            "type": "object",
            "properties": {
                "message": {
                    "type": "string"
                }
            }
        },
        output_example={
            "message": "All Animals created"
        }
    )
    def post(self):
        """
        POST the required parameters to create few new Animals instance.
        Each instance need this parameters
        * `name`: Name of the animal
        * `race`: Race of the animal
        * `age`: Age of the animal
        * `likes`: Likes of the animal
        """
        session = SESSION()
        animals_from_request = self.body.get('new_animals')
        animals_to_add = []

        for new_animal in animals_from_request:
            animals_to_add.append(Animal(
                name=new_animal.get('name'),
                race=new_animal.get('race'),
                age=new_animal.get('age') if new_animal.get('age') else 0,
                likes=new_animal.get('likes') if new_animal.get('likes') else 0
            ))

        session.add_all(animals_to_add)
        session.commit()
        return {
            "message": "All Animals created"
        }
