from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, CheckConstraint

Base = declarative_base()


class Animal(Base):
    __tablename__ = 'animals'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(20), index=True, nullable=False)
    race = Column(String(20), nullable=False)
    age = Column(Integer, default=0)
    likes = Column(Integer, default=0)

    CheckConstraint('age >= 0', name='age_positive')

    def __repr__(self):
        return "Animal(name={}, race={}, age={}, likes={}".format(
            self.name, self.race, self.age, self.likes
        )
